package org.mongodb;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

/**
 * Created by gerardo on 06/08/15.
 */
public class HelloWorldSparkStyle {
    public static void main(String[] args) {
        Spark.get("/",new Route(){
            @Override
            public Object handle(Request request,Response response) {
                return "hello world from spark";
            }
        });
    }
}
