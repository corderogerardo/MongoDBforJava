package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;

import static com.mongodb.client.model.Filters.gt;

/**
 * Created by Gerardo Cordero_2 on 8/21/2015.
 */
public class DeleteTest {
    public static void main(String[] args) {
        MongoClient cliente = new MongoClient();
        MongoDatabase db = cliente.getDatabase("course");
        MongoCollection<Document> col = db.getCollection("findWithFilterTest");
        col.drop();

        for (int i = 0; i<10;i++){
            col.insertOne(new Document().append("_id", i));
        }

        col.deleteMany(gt("_id",4));
        col.deleteOne(gt("_id",2));

        for(Document cur : col.find().into(new ArrayList<Document>())){
            System.out.println(cur);
        }
    }
}
