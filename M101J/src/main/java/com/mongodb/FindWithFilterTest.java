package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.*;

/**
 * Created by Gerardo Cordero_2 on 8/18/2015.
 */
public class FindWithFilterTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> collection = db.getCollection("findWithFilterTest");
        collection.drop();

        //insert 10 documents with two random integers
        for(int i=0;i<10;i++){
            collection.insertOne(new Document()
            .append("x",new Random().nextInt(2))
            .append("y",new Random().nextInt(100)));
        }
       // Bson filter = new Document("x",0).append("y",new Document("$gt",10));

        Bson filter = and(eq("x", 0),gt("y",10),lt("y",90));

        Bson projection = new Document("y",1).append("i", 1);

        // Proyecciones con la clase Projections
        Bson projectionDos = exclude("x", "_id");
        Bson projectionTres = exclude("y", "i");
        Bson ProyectionCuatro = fields(include("y", "i"), excludeId());

        List<Document> all = collection.find(filter).projection(ProyectionCuatro).into(new ArrayList<Document>());
        for(Document cur: all){
            System.out.println(cur);
        }
        long count = collection.count(filter);
        System.out.println(count);
    }
}
