package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Projections.*;
import static com.mongodb.client.model.Sorts.*;

/**
 * Created by Gerardo Cordero_2 on 8/19/2015.
 */
public class FindWidhSortSkipLimitTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> collection = db.getCollection("findWithSortTest");
        collection.drop();

        for (int i = 0; i<10;i++){
            for(int j = 0; j<10;j++){
                collection.insertOne(new Document().append("i",i).append("j",j));
            }
        }

        Bson projection = fields(include("i","j"),excludeId());
        Bson sort = new Document("i",1).append("j", 1); //ascending
        Bson sortDos = new Document("i",1).append("j",-1);//descending, -1 reversing
        Bson sortAsceding = ascending("i");
        Bson sortOrderBy = orderBy(ascending("i"), descending("j"));
        Bson sortij = descending("j","i");

        //Aqui tomamos todo el documento, buscamos, creamos una proyecciones, utilizamos la clase Sort,
        //y creamos varios criterios de orden
        //utilizamos limit y skip para colocar un limite y saltarnos valores en la busqueda o consulta.
        List<Document> all = collection.find().projection(projection).sort(sortij).skip(20).limit(50).into(new ArrayList<Document>());

        for(Document cur : all){
            System.out.println(cur);
        }
    }
}
