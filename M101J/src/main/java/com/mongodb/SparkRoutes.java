package com.mongodb;

import spark.Spark;
import spark.Route;
import spark.Response;
import spark.Request;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import java.util.Arrays;
import static spark.Spark.halt;

/**
 * Created by Gerardo Cordero_2 on 8/7/2015.
 */
public class SparkRoutes {
    public static void main(String[] args) {
        //configure freemarker
        final Configuration configuration = new Configuration();

        configuration.setClassForTemplateLoading(SparkRoutes.class,"/");


        Spark.get("/",new Route() {
            StringWriter writer = new StringWriter();
            @Override
            public Object handle(Request request,Response response) {
                try {
                    Map<String, Object> fruitsMap = new HashMap<String, Object>();
                    fruitsMap.put("fruits", Arrays.asList("apple", "orange", "banana", "peach"));

                    Template fruitPickerTemplate = configuration.getTemplate("hello.ftl");

                    fruitPickerTemplate.process(fruitsMap, writer);
                    return writer;
                } catch (Exception e) {
                    halt(500);
                    return null;
                }
            }
        });

        Spark.post("/favorite_fruit", new Route() {
            public Object handle(Request request, Response response) {
                String fruit = request.queryParams("fruit");
                if (fruit == null) {
                    return "Why don't you pick one";
                } else {
                    return "Your favorite fruit is " + fruit;
                }
            }
        });

        Spark.get("/test", new Route() {
            public Object handle(Request request, Response response){

                return "this is a test page\n";
            }
        });

        Spark.get("/echo/:thing", new Route() {
            public Object handle(Request request, Response response) {

                return request.params(":thing");
            }
        });

    }
}
