package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * Created by Gerardo Cordero_2 on 8/18/2015.
 */
public class InsertTest {
    public static void main(String[] args) {


        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("course");
        MongoCollection<Document> coll = db.getCollection("insertTest");

        coll.drop();

        Document gerardo = new Document("name", "Gerardo").append("age", 26)
                .append("profession", "programmer");
        //printJson(gerardo);
        coll.insertOne(gerardo);
        //printJson(gerardo);
    }
}

