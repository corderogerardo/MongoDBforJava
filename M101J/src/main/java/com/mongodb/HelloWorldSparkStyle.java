package com.mongodb;

import spark.*;
import spark.Response;

/**
 * Created by Gerardo Cordero_2 on 8/6/2015.
 */
public class HelloWorldSparkStyle {

    public static void main(String[] args) {
        Spark.get("/" , new Route(){
            @Override
            public Object handle(Request request, Response response){

                return "Hello World from Spark";


            }
        });
    }
}
