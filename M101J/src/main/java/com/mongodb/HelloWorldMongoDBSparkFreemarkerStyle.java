package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.bson.Document;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.io.StringWriter;

import static spark.Spark.halt;

/**
 * Created by Gerardo Cordero_2 on 8/21/2015.
 */
public class HelloWorldMongoDBSparkFreemarkerStyle {
    public static void main(String[] args) {
        final Configuration conf = new Configuration();
        conf.setClassForTemplateLoading(HelloWorldMongoDBSparkFreemarkerStyle.class, "/");

        MongoClient cliente = new MongoClient();

        MongoDatabase db = cliente.getDatabase("course");
        final MongoCollection<Document> collection = db.getCollection("hello");
        collection.drop();

        collection.insertOne(new Document("name", "MongoDB"));

        Spark.get("/",new Route(){
            @Override
            public Object handle(final Request request,final Response response){
                StringWriter writer = new StringWriter();
                try {
                    Template helloTemplate = conf.getTemplate("hello.ftl");
                    Document document = collection.find().first();
                    helloTemplate.process(document,writer);
                }catch (Exception e){
                    halt(500);
                    e.printStackTrace();

                }
                return writer;
            }

        });
    }
    }
