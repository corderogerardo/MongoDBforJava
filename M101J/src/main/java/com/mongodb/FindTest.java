package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerardo Cordero_2 on 8/18/2015.
 */
public class FindTest {
    public static void main(String[] args) {
        MongoClient cliente = new MongoClient();
        MongoDatabase db = cliente.getDatabase("course");
        MongoCollection<Document> coll = db.getCollection("findTest");

        coll.drop();

        //insert 10 documents

        for(int i=0;i<10;i++){
            coll.insertOne(new Document("x",i));
        }

        System.out.print("find one: ");
        Document first = coll.find().first();
        System.out.println(first);
        System.out.print("find all with into: ");
        List<Document> all = coll.find().into(new ArrayList<Document>());
        for(Document cur:all){
            System.out.println(cur);

        }
        System.out.print("find all with iteration: ");
        MongoCursor<Document> cursor = coll.find().iterator();
        try {
            while(cursor.hasNext()){
                Document cur = cursor.next();
                System.out.println(cur);
            }
        }finally {
            cursor.close();
        }

        System.out.print("count: ");
        long count = coll.count();
        System.out.print(count);

    }
}
