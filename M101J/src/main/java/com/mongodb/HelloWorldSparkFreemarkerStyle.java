package com.mongodb;

import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.*;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.halt;

/**
 * Created by Gerardo Cordero_2 on 8/6/2015.
 */
public class HelloWorldSparkFreemarkerStyle {
    public static void main(String[] args) {
        final Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(HelloWorldFreemarkerStyle.class,"/");

        Spark.get("/", new Route() {
            @Override
            public Object handle(Request request, spark.Response response) {
                StringWriter writer = new StringWriter();
                try {
                    Template helloTemplate = configuration.getTemplate("hello.ftl");
                    Map<String,Object> helloMap = new HashMap<String, Object>();
                    helloMap.put("name","Gerardo Cordero");
                    helloTemplate.process(helloMap, writer);

                    System.out.println(writer);
                }catch (Exception e){
                    halt(500);
                    e.printStackTrace();

                }

                return writer;

            }
        });
    }
}
