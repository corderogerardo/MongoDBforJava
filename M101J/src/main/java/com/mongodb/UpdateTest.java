package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;

import java.util.ArrayList;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;

/**
 * Created by Gerardo Cordero_2 on 8/20/2015.
 */
public class UpdateTest {
    public static void main(String[] args) {
        MongoClient cliente = new MongoClient();
        MongoDatabase db = cliente.getDatabase("course");
        MongoCollection<Document> col = db.getCollection("findWithFilterTest");
        col.drop();

        for (int i = 0; i<10;i++){
          col.insertOne(new Document().append("_id",i).append("x",i));
        }

        //col.replaceOne(eq("x",5), new Document("_id",5).append("x",20).append("update",true));
        //col.updateOne(eq("_id",9), new Document("$set",new Document("x",20)),new UpdateOptions().upsert(true));
        col.updateMany(gte("_id",5), new Document("$inc", new Document("x",1)));
        for(Document cur : col.find().into(new ArrayList<Document>())){
            System.out.println(cur);
        }
    }
}
