package course;

import java.util.Date;

/**
 * Created by gerardo on 23/08/15.
 */
public class PersonaDAO2 {

        private String cedula;
        private String nombre;
        private String apellido;
        private Date fechaNacimiento;
        private String email;
        private String direccion;
        private String telefono;

        public PersonaDAO2(String cedula, String nombre, String apellido, Date fechaNacimiento, String email, String direccion, String telefono) {
            this.cedula = cedula;
            this.nombre = nombre;
            this.apellido = apellido;
            this.fechaNacimiento = fechaNacimiento;
            this.email = email;
            this.direccion = direccion;
            this.telefono = telefono;
        }

        public PersonaDAO2() {
        }

        public String getCedula() {
            return cedula;
        }

        public void setCedula(String cedula) {
            this.cedula = cedula;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public Date getFechaNacimiento() {
            return fechaNacimiento;
        }

        public void setFechaNacimiento(Date fechaNacimiento) {
            this.fechaNacimiento = fechaNacimiento;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDireccion() {
            return direccion;
        }

        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }

        public String getTelefono() {
            return telefono;
        }

        public void setTelefono(String telefono) {
            this.telefono = telefono;
        }
    }

